#include <EEPROM.h>
#include <SendOnlySoftwareSerial.h>
#include <FreqCounter.h>
#include <LiquidCrystal_I2C.h>
#include <SdFat.h>
#include <Adafruit_BMP085.h>
#include <DS1307RTC.h>
#include <Time.h>
#include <Wire.h>
#include <BH1750FVI.h>
#include "DHT.h"
#include "common.h"


#define AP_SSID			"linksys"
#define AP_PASSWD		"RTFMYFM78"
#define AP_SERVER_IP	"192.168.1.10"
#define AP_SERVER_PORT	7171

#define TX					0	// pin D0 is TX for Serial [N/A]
#define RX					1	// pin D1 is RX for Serial [N/A]
#define DHT_D				2	// pin D2 is DHT data
#define SW_0				3	// pin D3 is N/U
#define SW_1				4	// pin D4 is N/U
#define WIND_SPEED			5	// pin D5 is wind speed meter
#define SW_2				6	// pin D6 is LCD backlight enable
#define RAIN_SENSOR_D		7	// pin D7 is rain sensor trigger
#define HB_LED				8	// pin D8 is the heartbeat LED
#define SW_TX				9	// pin D9 as software serial TX
#define SD_CE				10	// pin D10 is the SPI CE signal of SD card
// pin D11,D12,D13 are for SPI bus
// Analog pins
//
#define RAIN_SENSOR_A		0	// pin A0 is for analog reading of rain sensor
#define WIND_DIRECTION		1	// pin A1 is for analog reading of wind direction
#define BATT_MON			3	// pin A3 is for RTC battery monitor

// I2C addresses definitions
//
#define I2C_ADDR_BH1750		0x23
#define I2C_ADDR_LCD		0x27
#define I2C_ADDR_EEPROM		0x50
#define I2C_ADDR_DS1307		0x68
#define I2C_ADDR_BMP180		0x77
const char i2c_addresses[] = { I2C_ADDR_BH1750, I2C_ADDR_LCD, I2C_ADDR_EEPROM, I2C_ADDR_DS1307, I2C_ADDR_BMP180 };

const char *wind_dir[] = { "N", "NE", "E", "NW", "SE", "W", "SW", "S" };
typedef enum {
	WIND_DIR_N = 0,
	WIND_DIR_NE,
	WIND_DIR_E,
	WIND_DIR_NW,
	WIND_DIR_SE,
	WIND_DIR_W,
	WIND_DIR_SW,
	WIND_DIR_S
} en_wind_direction;

typedef enum {
	WIND_DIR_N_VAL = 107,
	WIND_DIR_NE_VAL = 186,
	WIND_DIR_E_VAL = 309,
	WIND_DIR_NW_VAL = 472,
	WIND_DIR_SE_VAL = 640,
	WIND_DIR_W_VAL = 786,
	WIND_DIR_SW_VAL = 883,
	WIND_DIR_S_VAL = 1023
} en_wind_direction_vals;

// Switch options definitions
//
//typedef enum {
//SW_OPT_USE_SD_CARD = 1,
//SW_OPT_SERIAL_DEBUG = 2,
//SW_OPT_LCD_BACKLIGHT = 4
//} en_sw_options;

//#define IS_LCD_ENABLED		(sys.options & SW_OPT_LCD_BACKLIGHT)
//#define IS_SDCARD_ENABLED	(sys.options & SW_OPT_USE_SD_CARD)
//#define IS_SERIAL_ENABLED	(sys.options & SW_OPT_SERIAL_DEBUG)
//#define READ_SW_OPTIONS		~(0xF0 | (digitalRead(SW_2) << 2) | (digitalRead(SW_1) << 1) | (digitalRead(SW_0)))

#define IS_LCD_ENABLED		(digitalRead(SW_2))
#define IS_SERIAL_ENABLED	(1)
#define IS_SDCARD_ENABLED	(0)

// UART definitions
//
#define SOFT_SERIAL
#define DEBUG
//#define USE_SD

#ifdef SOFT_SERIAL
#define UART	dbg
SendOnlySoftwareSerial dbg(SW_TX);	// use D8 for serial port
#else
#define UART	Serial
#endif

#ifdef DEBUG
#define DEBUG(X)		if IS_SERIAL_ENABLED UART.print(X)
#define DEBUGW(X)		if IS_SERIAL_ENABLED UART.write(X)
#define DEBUGF(X)		if IS_SERIAL_ENABLED UART.print(F(X))
#define DEBUGL(X)		if IS_SERIAL_ENABLED UART.println(X)
#define DEBUGLO(X, O)	if IS_SERIAL_ENABLED UART.println(X, O)
#define DEBUGLF(X)		if IS_SERIAL_ENABLED UART.println(F(X))
#else
#define DEBUG(X)		{}
#define DEBUGW(X)		{}
#define DEBUGF(X)		{}
#define DEBUGL(X)		{}
#define DEBUGLO(X, O)	{}
#define DEBUGLF(X)		{}
#endif


typedef struct {
	uint16_t	eeprom_header;
	char		wifi_ssid[40];
	char		wifi_passwd[32];
	char		udp_ip[16];
	uint16_t	port;
	uint16_t	eeprom_cs;
} tp_conf;

tp_conf conf;


// Sensors definitions
//
typedef struct {
	float		temp_C;
	float		temp_F;
	float		humidity;
	float		heatindex;
	uint16_t	lux;
	float		pressure;
	float		pressure_sea_level;
	float		altitude;
	float		bmp180_temp;
	uint16_t	rain_adc;	// Analog value of rain sensor
	uint8_t		rain;		// Digital value of rain sensor. 0: no rain, 1: rain
	uint32_t	wind_speed;	// wind speed (counts/20 is the m/s value)
	uint8_t		wind_direction;	// wind direction
	uint16_t	wind_direction_val;	// wind direction
	uint16_t	rtc_batt;	// RTC's battery
} tp_sensors_values;

typedef enum {
	SENSOR_STATUS_OK,
	SENSOR_STATUS_FOUND,
	SENSOR_STATUS_NOT_FOUND,
	SENSOR_STATUS_FAIL
} en_sensor_status;

typedef struct {
	uint8_t		DHTxx;		// Temp/Humidity sensor
	uint8_t		BH1750;		// Light sensor
	uint8_t		LCD20x4;	// LCD
	uint8_t		EEPROM;		// RTC EEPROM
	uint8_t		RTC;		// DS1307 RTC
	uint8_t		BMP180;		// Barometric sensor
	uint8_t		SDCARD;
} tp_sensor_status;


// System definitions
//
typedef struct {
	uint8_t				options;		// see en_sw_options
	tp_sensors_values	sensor_values;
	tp_sensor_status	sensor_status;
} tp_system;

tp_system sys;

#define LOG_EVERY_SECS	60L
time_t timestamp = 0;
time_t log_timestamp = 0;

// Global declarations
//
DHT				snr_dht(DHT_D, DHT22);	// DHT sensor for temp/humidity
BH1750FVI		snr_bh1750;				// Light sensor
Adafruit_BMP085 snr_bmp;				// Barometric pressure sensor

#ifdef USE_SD
SdFat			sd;						// SD card
SdFile			log_file;				// SD file
char			fileName[8] = "LOG.CSV";
const uint8_t	chipSelect = SS;		// pin used for CE for SD SPI

// Error messages stored in flash
//
#define error(msg) error_P(PSTR(msg))
void error_P(const char* msg) {
	sd.errorHalt_P(msg);
}
#endif

tmElements_t	tm;						// time struct

#define LCD_WRITE(LINE, TEXT)	lcd.setCursor(0, LINE);\
								lcd.print(TEXT)
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);	// Init an LCD obj on I2C address 0x27 and 4x20



// Function prototypes
//
void print2digits(int number);
void I2Cupdate_status(uint8_t address, uint8_t status);
void I2Cscan();


// Init/Setup
//
void setup() {

	// Set up heartbeat led
	pinMode(HB_LED, OUTPUT);
	pinMode(RAIN_SENSOR_D, INPUT_PULLUP);
	pinMode(SW_0, INPUT_PULLUP);
	pinMode(SW_1, INPUT_PULLUP);
	pinMode(SW_2, INPUT_PULLUP);


	// First time read options
	//sys.options = READ_SW_OPTIONS;

	// default values
	//
	memset(&sys.sensor_values, 0, sizeof(tp_sensors_values));

	/* add setup code here */
#ifdef DEBUG
	UART.begin(9600);
#endif

	Serial.begin(9600);	// start serial port for WIFI

	DEBUGF("Weather Station Version v."); DEBUGL(FW_VERSION);


	//read conf from EEPROM
	memset(&conf, 0, sizeof(tp_conf));
	CONF_Read();


	// Init LCD
	lcd.begin(20, 4);
	if IS_LCD_ENABLED lcd.backlight();
	else lcd.noBacklight();

	lcd.clear();
	LCD_WRITE(0, " WeatherStation v.1 ");
	LCD_WRITE(1, "--------------------");


#ifndef USE_SD
	ESP8266_Init();
#endif

	// Scan I2C bus for devices
	I2Cscan();

	// Init Temp/Humidity sensor DHTxx
	snr_dht.begin();

	// Init light sensor BH1750VI
	if (sys.sensor_status.BH1750 == SENSOR_STATUS_FOUND) {
		snr_bh1750.begin();
		snr_bh1750.SetAddress(Device_Address_L); //Address 0x23
		snr_bh1750.SetMode(Continuous_H_resolution_Mode);
		//I2Cupdate_status(I2C_ADDR_BH1750, SENSOR_STATUS_OK);
	}

	// Init barometric sensor BMP180
	if (sys.sensor_status.BMP180 == SENSOR_STATUS_FOUND) {
		sys.sensor_status.BMP180 = SENSOR_STATUS_OK;
		if (!snr_bmp.begin()) {
			DEBUGLF("Could not find a valid BMP085 sensor, check wiring!\r\n");
			sys.sensor_status.BMP180 = SENSOR_STATUS_FAIL;
		}
	}

	// Check if RTC is active
	if (sys.sensor_status.RTC == SENSOR_STATUS_FOUND) {
		sys.sensor_status.RTC = SENSOR_STATUS_OK;
		if (!RTC.get()) {
			if (RTC.chipPresent()) {
				DEBUGLF("Reseting RTC time...");
				RTC.set(1424547300L);
			}
			else {
				sys.sensor_status.RTC = SENSOR_STATUS_FAIL;
			}
		}
	}

#ifdef USE_SD
	// Check if SD logging is enabled from SW options
	if (IS_SDCARD_ENABLED) {
		delay(400);  // catch Due reset problem

		// Init SD Card
		if (!sd.begin(chipSelect, SPI_HALF_SPEED)) {
			LCD_WRITE(3, "SD failed to init...");
			sd.initErrorHalt("Could not begin!");
		}

		//bool file_not_exists = false;
		if (!log_file.exists(fileName)) {
			// file doesn't exist then create header
			file_not_exists = true;
		}
		if (!log_file.open(fileName, FILE_WRITE)) {
			sd.errorHalt("opening log for write failed");
			LCD_WRITE(3, "Opening log failed..");
		}
		if (file_not_exists) {
			// write header [timestamp],[temp_C],[humidity],[heat index],[lux],[bmp180_temp],[pressure],[altitude],[temp_F]
			log_file.print(F("timestamp,Temp (C),Humidity (%),Heat Index (C),Light (lux),BMP180 Temp (C), Pressure (Pa),Altitude (m),Temp (F)"));
			log_file.println();
			if (!log_file.sync() || log_file.getWriteError()) Serial.println("write error");
		}
	}
#endif
}

void loop() {

	uint8_t previous_opts = sys.options;
	uint8_t udp_buffer[100];

	digitalWrite(HB_LED, LOW);
	delay(1000);

	DEBUGL(); DEBUGL();

	//sys.options = READ_SW_OPTIONS;
	//DEBUGF("Options: 0x"); DEBUGLO(sys.options, HEX);

	// Control LCD backlight
	//
	if (IS_LCD_ENABLED) lcd.backlight();
	else lcd.noBacklight();

	// Wait a few seconds between measurements.
	//
	digitalWrite(HB_LED, HIGH);

	// Anemometer
	//
	FreqCounter::f_comp = 8;             // Set compensation to 12
	FreqCounter::start(5000);           // Start counting with gatetime of 100ms
	while (FreqCounter::f_ready == 0);	// wait until counter ready
	sys.sensor_values.wind_speed = FreqCounter::f_freq;            // read result
	DEBUGF("Freq: "); DEBUGL(sys.sensor_values.wind_speed);

	// Wind direction
	//
	sys.sensor_values.wind_direction_val = analogRead(WIND_DIRECTION);

	if (sys.sensor_values.wind_direction_val <= WIND_DIR_N_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_N;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_NE_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_NE;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_E_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_E;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_NW_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_NW;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_SE_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_SE;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_W_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_W;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_SW_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_SW;
	}
	else if (sys.sensor_values.wind_direction_val <= WIND_DIR_S_VAL) {
		sys.sensor_values.wind_direction = WIND_DIR_S;
	}
	DEBUGF("Wind direction: "); DEBUGL(sys.sensor_values.wind_direction);

	// monitor RTC battery
	sys.sensor_values.rtc_batt = analogRead(BATT_MON);
	DEBUGF("Battery Volts: "); DEBUGL(sys.sensor_values.rtc_batt);

	// Read rain sensor
	sys.sensor_values.rain_adc = analogRead(RAIN_SENSOR_A);
	DEBUGF("Rain: "); DEBUGL(sys.sensor_values.rain_adc);

	sys.sensor_values.rain = digitalRead(RAIN_SENSOR_D);	// active low
	DEBUGF("Rain(D): "); DEBUGL(sys.sensor_values.rain);

	if (sys.sensor_status.RTC == SENSOR_STATUS_OK) {
		RTC.read(tm);
		timestamp = RTC.get();
		DEBUGF("Ok, Time = ");
		print2digits(tm.Hour);
		DEBUGW(':');
		print2digits(tm.Minute);
		DEBUGW(':');
		print2digits(tm.Second);
		DEBUGF(", Date (D/M/Y) = ");
		DEBUG(tm.Day);
		DEBUGW('/');
		DEBUG(tm.Month);
		DEBUGW('/');
		DEBUG(tmYearToCalendar(tm.Year));
		DEBUGL();
	}
	else {
		if (RTC.chipPresent()) {
			DEBUGF("The DS1307 is stopped.  Please run the SetTime");
			DEBUGLF("example to initialize the time and begin running.");
			DEBUGL();
		}
		else {
			DEBUGLF("DS1307 read error!  Please check the circuitry.");
			DEBUGL();
		}
	}


	// Reading temperature or humidity takes about 250 milliseconds!
	// Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
	sys.sensor_values.humidity = snr_dht.readHumidity();        // Read humidity
	sys.sensor_values.temp_C = snr_dht.readTemperature();       // Read temperature as Celsius
	sys.sensor_values.temp_F = snr_dht.readTemperature(true);   // Read temperature as Fahrenheit

	// Check if any reads failed and exit early (to try again).
	if (isnan(sys.sensor_values.humidity) || isnan(sys.sensor_values.temp_C) || isnan(sys.sensor_values.temp_F)) {
		// LOG error
		DEBUGLF("Failed to read from DHT sensor!");
		sys.sensor_status.DHTxx = SENSOR_STATUS_FAIL;
	}
	else {
		sys.sensor_status.DHTxx = SENSOR_STATUS_OK;
		sys.sensor_values.heatindex = snr_dht.convertFtoC(snr_dht.computeHeatIndex(sys.sensor_values.temp_F, sys.sensor_values.humidity));  // Compute heat index

		// Must send in temp in Fahrenheit!
		DEBUGF("Humidity: "); DEBUGL(sys.sensor_values.humidity);
		DEBUGF("Temp C: "); DEBUG(sys.sensor_values.temp_C); DEBUGLF(" *C ");
		DEBUGF("Temp F: "); DEBUG(sys.sensor_values.temp_F); DEBUGLF(" *F");
		DEBUGF("Heat index: "); DEBUG(sys.sensor_values.heatindex); DEBUGLF(" *C");
	}

	// Lightmeter
	//
	sys.sensor_values.lux = snr_bh1750.GetLightIntensity(); // Get Lux value
	DEBUGF("Light: ");
	DEBUG(sys.sensor_values.lux);
	DEBUGLF(" lux");

	if (sys.sensor_status.BMP180 == SENSOR_STATUS_OK) {
		sys.sensor_values.bmp180_temp = snr_bmp.readTemperature();
		sys.sensor_values.pressure = snr_bmp.readPressure();
		sys.sensor_values.altitude = snr_bmp.readAltitude();


		DEBUGF("Temperature = "); DEBUG(sys.sensor_values.bmp180_temp); DEBUGLF(" *C");
		DEBUGF("Pressure = "); DEBUG(sys.sensor_values.pressure); DEBUGLF(" Pa");

		// Calculate altitude assuming 'standard' barometric
		// pressure of 1013.25 millibar = 101325 Pascal
		DEBUGF("Altitude = "); DEBUG(sys.sensor_values.altitude); DEBUGLF(" meters");
	}

	// Display data format on LCD
	//    01234567890123456789
	// 0: C:xx.xx*C HI:xx.xx%
	// 1: H:xx.xx%  PC:xx.xx*C
	// 2: L:xxxxlux PA:xxx.xxm
	// 3: P:xxxxx.xx Pa
	//
	lcd.clear();
	lcd.home();
	lcd.write("C:"); lcd.print(sys.sensor_values.temp_C); lcd.write("*C");
	lcd.setCursor(10, 0);
	lcd.write("HI:"); lcd.print(sys.sensor_values.heatindex); lcd.write("*C");
	lcd.setCursor(0, 1);
	lcd.write("H:"); lcd.print(sys.sensor_values.humidity); lcd.write("%");
	lcd.setCursor(10, 1);
	lcd.write("PC:"); lcd.print(sys.sensor_values.bmp180_temp); lcd.write("*C");
	lcd.setCursor(0, 2);
	lcd.write("L:"); lcd.print(sys.sensor_values.lux); lcd.write("lux");
	lcd.setCursor(10, 2);
	lcd.write("PA:"); lcd.print(sys.sensor_values.altitude); lcd.write("m");
	lcd.setCursor(0, 3);
	lcd.write("P:"); lcd.print(sys.sensor_values.pressure); lcd.write("Pa");
	lcd.setCursor(13, 3);
	lcd.write("W:"); lcd.print(sys.sensor_values.wind_speed); lcd.write(wind_dir[sys.sensor_values.wind_direction]);


#ifdef USE_SD
	// SD card log format
	// [timestamp],[options],[temp_C],[humidity],[heat index],[lux],[bmp180_temp],[pressure],[altitude],[wind speed],[wind direction],[rtc batt],[rain adc],[rain]
	//
	if (IS_SDCARD_ENABLED) {
		if (log_file.isOpen() && ((timestamp - log_timestamp) > LOG_EVERY_SECS)) {

			log_timestamp = timestamp;
			DEBUGF("Writing to log...");
			log_file.print(timestamp); log_file.write(';');
			log_file.print(sys.options); log_file.write(';');
			log_file.print(sys.sensor_values.temp_C); log_file.write(';');
			log_file.print(sys.sensor_values.humidity); log_file.write(';');
			log_file.print(sys.sensor_values.heatindex); log_file.write(';');
			log_file.print(sys.sensor_values.lux); log_file.write(';');
			log_file.print(sys.sensor_values.bmp180_temp); log_file.write(';');
			log_file.print(sys.sensor_values.pressure); log_file.write(';');
			log_file.print(sys.sensor_values.altitude); log_file.write(';');
			log_file.print(sys.sensor_values.wind_speed); log_file.write(';');
			log_file.print(sys.sensor_values.wind_direction); log_file.write(';');
			log_file.print(sys.sensor_values.rtc_batt); log_file.write(';');
			log_file.print(sys.sensor_values.rain_adc); log_file.write(';');
			log_file.print(sys.sensor_values.rain); log_file.write(';');
			log_file.println();

			if (!log_file.sync() || log_file.getWriteError()) DEBUGLF("write error");
		}
	}
#else
	// If connection status is connected then send data
	if (ESP8266_CIPSTATUS()) {
		ESP8266_CIPSENDSingle((uint8_t*)&sys.sensor_values, sizeof(tp_sensors_values));
	}
	// else bind to UDP port
	else {
		ESP8266_Init();
	}
#endif
}



void CONF_Read(void)
{
	uint8_t i;
	uint8_t * p = (uint8_t*)&conf;
	uint8_t valid_data = false;

	DEBUGF("EEPROM content:");
	for (i = 0; i<sizeof(tp_conf); i++) {
		p[i] = EEPROM.read(i);
		DEBUG(p[i]);
	}

	if (conf.eeprom_header == 0xABCD) {
		// probably valid data
		uint16_t cs = 0;
		for (i = 0; i<sizeof(tp_conf) - 2; i++) {
			cs += p[i];
		}
		if (cs == conf.eeprom_cs) {
			//valid data
			valid_data = true;
		}
	}

	// if not valid data found or false cs the load defaults
	if (!valid_data) {
		// first time used. Load defaults
		conf.eeprom_header == 0xABCD;
		strcpy(conf.wifi_ssid, AP_SSID);
		strcpy(conf.wifi_passwd, AP_PASSWD);
		strcpy(conf.udp_ip, AP_SERVER_IP);
		conf.port = AP_SERVER_PORT;
		for (i = 0; i<sizeof(tp_conf) - 2; i++) {
			conf.eeprom_cs += p[i];
		}
		// Save to EEPROM
		for (i = 0; i<sizeof(tp_conf); i++) {
			EEPROM.write(i, p[i]);
		}
	}

	DEBUGF("header:"); DEBUGL(conf.eeprom_header);
	DEBUGF("ssid:"); DEBUGL(conf.wifi_ssid);
	DEBUGF("pswd:"); DEBUGL(conf.wifi_passwd);
	DEBUGF("udp ip:"); DEBUGL(conf.udp_ip);
	DEBUGF("port:"); DEBUGL(conf.port);
	DEBUGF("CS:"); DEBUGL(conf.eeprom_cs);
}

uint8_t ESP8266_Init(void)
{
	uint8_t result = 0;	// by default no wifi

	//Init ESP8266
	LCD_WRITE(3, "Init WiFi");
	delay(1000);
	rx_empty();
	if (ESP8266_RST()) {
		bool resp = false;

		lcd.setCursor(0, 3);
		lcd.write("WiFi: found         ");

		DEBUGL("Reseting WiFi");
		while (!ESP8266_AT()) {
			DEBUGL("No OK");
			delay(1000);
		}
		resp = ESP8266_CWMODE(3);
		if (resp) {
			LCD_WRITE(3, "WiFi: Set to mode 3 ");
		}
		delay(1000);

		resp = ESP8266_CWJAP(conf.wifi_ssid, conf.wifi_passwd);
		if (resp) {
			lcd.setCursor(0, 3);
			LCD_WRITE(3, "WiFi: Connected AP ");
		}
		delay(1000);

		ESP8266_CIPMUX(0);
		if (resp) {
			lcd.setCursor(0, 3);
			LCD_WRITE(3, "WiFi: Set mux to 0 ");
		}
		delay(1000);

		ESP8266_CIPSTART(conf.udp_ip, conf.port);
		if (resp) {
			lcd.setCursor(0, 3);
			LCD_WRITE(3, "WiFi: UDP port bind");
		}
		delay(1000);
		result = 1;
	}
	else {
		LCD_WRITE(3, "WiFi not found...   ");
		delay(2000);
	}
	return(result);
}

void rx_empty(void)
{
	while (Serial.available() > 0) {
		Serial.read();
	}
}

String recvString(String target, uint32_t timeout = 1000)
{
	String data;
	char a;
	unsigned long start = millis();
	while (millis() - start < timeout) {
		while (Serial.available() > 0) {
			a = Serial.read();
			if (a == '\0') continue;
			data += a;
		}
		if (data.indexOf(target) != -1) {
			break;
		}
	}
	DEBUGL(data);
	return data;
}

bool recvFind(String target, uint32_t timeout)
{
	String data_tmp;
	data_tmp = recvString(target, timeout);
	if (data_tmp.indexOf(target) != -1) {
		return true;
	}
	return false;
}

bool ESP8266_AT(void)
{
	rx_empty();
	Serial.println("AT");
	return recvFind("OK", 1000);
}

bool ESP8266_RST(void)
{
	rx_empty();
	Serial.println("AT+RST");
	return recvFind("ready", 10000);
}

bool ESP8266_CWMODE(int mode)
{
	String data;

	rx_empty();
	Serial.print("AT+CWMODE=");
	Serial.println(mode);

	data = recvString("OK", 3000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CWJAP(String ssid, String pwd)
{
	String data;
	rx_empty();
	Serial.print("AT+CWJAP=\"");
	Serial.print(ssid);
	Serial.print("\",\"");
	Serial.print(pwd);
	Serial.println("\"");

	data = recvString("OK", 10000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CIPMUX(int mux)
{
	String data;
	rx_empty();
	Serial.print("AT+CIPMUX=");
	Serial.println(mux);

	data = recvString("OK", 3000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CIPSTART(String ip, int port)
{
	String data;
	rx_empty();
	Serial.print("AT+CIPSTART=\"UDP\",\"");
	Serial.print(ip);
	Serial.print("\",");
	Serial.println(port);

	data = recvString("OK", 3000);
	if (data.indexOf("OK") != -1) {
		return true;
	}
	return false;
}

bool ESP8266_CIPSTATUS(void)
{
	String data;
	bool result = false;

	delay(100);
	rx_empty();
	Serial.println("AT+CIPSTATUS");
	data = recvString("\r\n\r\nOK");
	int index = data.indexOf("+CIPSTATUS:");
	DEBUGF("index = "); DEBUGL(index);
	index = data.indexOf("STATUS:");
	DEBUGF("index = "); DEBUGL(index);
	if (index) {
		int status = data.substring(index + 7, index + 7 + 1).toInt();
		DEBUGF("UDP status = "); DEBUGL(status);
		if (status == 5) {
			result = true;
		}
	}
	return(result);
}

bool ESP8266_CIPSENDSingle(const uint8_t *buffer, uint32_t len)
{
	rx_empty();
	Serial.print("AT+CIPSEND=");
	Serial.println(len);
	if (recvFind(">", 5000)) {
		rx_empty();
		for (uint32_t i = 0; i < len; i++) {
			Serial.write(buffer[i]);
		}
		return recvFind("SEND OK", 10000);
	}
	return false;
}



void print2digits(int number) {
	if (number >= 0 && number < 10) {
		Serial.write('0');
	}
	DEBUG(number);
}

void I2Cupdate_status(uint8_t address, uint8_t status)
{
	switch (address) {
	case I2C_ADDR_BH1750:
		sys.sensor_status.BH1750 = status;
		break;
	case I2C_ADDR_LCD:
		sys.sensor_status.LCD20x4 = status;
		break;
	case I2C_ADDR_EEPROM:
		sys.sensor_status.EEPROM = status;
		break;
	case I2C_ADDR_DS1307:
		sys.sensor_status.RTC = status;
		break;
	case I2C_ADDR_BMP180:
		sys.sensor_status.BMP180 = status;
		break;
	};
}

void I2Cscan()
{
	uint8_t count = 0;
	uint8_t addressStart = 0;
	uint8_t addressEnd = 127;
	uint16_t speed[4] = { 100, 200, 400, 800 };
	uint8_t found[4];

	LCD_WRITE(2, "Scanning I2C devices");
	//DEBUGLF("Scanning I2C devices"));
	delay(1000);

	for (uint8_t i = 0; i<(sizeof(i2c_addresses) / sizeof(i2c_addresses[0])); i++)
	{
		uint8_t found_speeds = 0;
		for (uint8_t s = 0; s < 4; s++)	// do check for all speeds
		{
			bool fnd = false;
			Wire.setClock(speed[s] * 1000);
			Wire.beginTransmission(i2c_addresses[i]);
			found[s] = (Wire.endTransmission() == 0);
			fnd |= found[s];
			if (fnd) found_speeds |= (1 << s);
		}
		if (found_speeds) {
			lcd.setCursor(4 * i, 3); lcd.print(i2c_addresses[i], HEX);
			if (found_speeds == 0x0F) lcd.write('+');
			else lcd.write('-');
			I2Cupdate_status(i2c_addresses[i], SENSOR_STATUS_FOUND);
			//DEBUGF("Found:");Serial.println(i2c_addresses[i], HEX);
		}
		else {
			I2Cupdate_status(i2c_addresses[i], SENSOR_STATUS_NOT_FOUND);
		}
	}
	delay(5000);
}
